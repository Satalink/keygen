package keygen;

public class CodeCreate{
    Interface iface;
    public CodeCreate(Interface face){
        iface = face;face = null;
    }
    public String AlphaCode(){
        int size = 128;
        String returncode = "";
        for(int i=0;i < size;i++){
            int y = 5;
            String chrString = "";
            while(chrString.length() < y+1){
                int f = iface.getSystem().genRandomInt(20);
                char c;
                if(f < 5){
                    c = (char)(iface.getSystem().genRandomInt(48, 57) & 0xFF);
                } else {
                    c = (char)(iface.getSystem().genRandomInt(65, 90) & 0xFF);
                }
                if(chrString.contains((String.valueOf(c)))){
                    y--;
                } else {
                    chrString += c;
                }
            }
            if(i<size-1){
                returncode+=(chrString+",");
            } else {
                returncode+=(chrString);
            }
        }
        return(returncode);
    }
    public String getKeyCode(){
        return(AlphaCode());
    }
}
