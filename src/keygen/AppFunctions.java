package keygen;

public class AppFunctions {
    Interface iface;
    public AppFunctions(Interface face){
        iface = face;face = null;
    }
    protected boolean addUser2DB(String user, String regKey, String keyCrypt){
        boolean status = false;
        java.util.Map dbMap = new java.util.HashMap<String, Integer>();
        dbMap.put("Recycle Bin Manager",1);
        dbMap.put("Digital Shredder",2);
        String app = iface.getGUI().getSelectedApplication().toString();
        int dbID = Integer.valueOf(dbMap.get(app).toString());
        //Log User Added
        String ipaddr = iface.getSystem().getIPaddress();
        String sql;
        sql = "INSERT INTO `LOG` (`ENTRY`,`USER`,`TIMESTAMP`) "+
              "VALUES ('Created: "+user+" for "+app+"','Satalink Soft',CURRENT_TIMESTAMP)";
        status = iface.getDB().Statement(0, sql);
        if(iface.getDB().Connect(dbID)){
            if(!iface.getDB().doesValueExist("USERS", "NAME", user)){
                sql = "INSERT INTO `USERS` (`NAME`,`STATE`,`KEYCRYPT`,`TIMESTAMP`) "+
                  "VALUES ('"+user+"','AVAILABLE','"+keyCrypt+"',CURRENT_TIMESTAMP)";
            } else {
                sql = "UPDATE `USERS` "+
                        "SET `STATE` = 'AVAILABLE',`KEYCRYPT` = '"+keyCrypt+"' "+
                        "WHERE `NAME` = '"+user+"'";
            }
            status = iface.getDB().Statement(dbID, sql);
        }
        return(status);
    }
    protected java.io.File registrations = new java.io.File("C:\\Temp\\development\\NB_Projects\\RecycleBinManager\\RecycleBinManager Website\\siteregistrations.xml");
    protected String generateKey(String uNam) {
        java.util.Random rnd = new java.util.Random();
        byte[] userName = uNam.toUpperCase().getBytes();
        String keyString = new CodeCreate(iface).getKeyCode();
        String[] keyArray = keyString.split(",");
        String keys = getUserKeys(rnd, keyArray, userName);
        String regkey = "";
        String[] charset = keys.split("\\|");
        for(int c=0;c < charset.length;c++) {
            int pos = rnd.nextInt(charset[c].length());
            regkey += charset[c].substring(pos, pos+1);
        }
        if(!addUser2DB(uNam,regkey,keyString)) regkey = "DB or Network ERROR!";
        return(regkey);
    }

    protected String getUserKeys(java.util.Random rnd, String[] bMap, byte[] bNam) {
        boolean f = false;
        String nKey = "";
        for(int b=0;b < bNam.length;b++) {
            int x = bNam[b];
            if      (x>47 && x<58) {
                nKey += bMap[x]+"|";
            }
            else if (x>64 && x<91) {
                nKey += bMap[x]+"|";
            }
        }
         return(nKey.substring(0, nKey.length()-1));
    }
    protected void putKeyInClipboard(final String key) {
        java.awt.datatransfer.Clipboard clipboard = java.awt.Toolkit.getDefaultToolkit().getSystemClipboard();
        java.awt.datatransfer.Transferable xfer = new java.awt.datatransfer.Transferable() {
            @Override
            public Object getTransferData(java.awt.datatransfer.DataFlavor flavor) throws java.awt.datatransfer.UnsupportedFlavorException ,java.io.IOException {
                if (flavor.equals(java.awt.datatransfer.DataFlavor.stringFlavor)) {
                    return key;
                }
                throw new java.awt.datatransfer.UnsupportedFlavorException(flavor);
            }
            @Override
            public java.awt.datatransfer.DataFlavor[] getTransferDataFlavors() {
                return new java.awt.datatransfer.DataFlavor[] { java.awt.datatransfer.DataFlavor.stringFlavor };
            }
            @Override
            public boolean isDataFlavorSupported(java.awt.datatransfer.DataFlavor flavor) {
                return flavor.equals(java.awt.datatransfer.DataFlavor.stringFlavor);
            }
        };
        clipboard.setContents(xfer, null);
    }
    protected void writeLog(java.util.Properties props){
        XMLConfig regConfig = new XMLConfig();
        try {
            regConfig.writeProperties(props, registrations);
        } catch (java.io.FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
}

