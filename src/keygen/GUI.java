package keygen;

public class GUI extends javax.swing.JFrame {
    Interface iface;
    java.util.Properties properties;
    javax.swing.table.DefaultTableModel jTable_RegistryModel;
    String application;
    /** Creates new form GUI */
    public GUI(Interface face) {
        try {
            initComponents();
            iface = face;
            face = null;
            setIconImage(new javax.swing.ImageIcon(iface.getSystem().getResource("/resources/KeyGen_Icon.png")).getImage());
            setTitle("KeyGen: Satalink Soft");
            jCheckBox_Clipboard.setSelected(false);
            jCheckBox_Email.setSelected(true);
            setLocation(200, 300);
            java.awt.event.MouseListener jTable_RegistryMouseListener = new java.awt.event.MouseAdapter() {

                @Override
                public void mousePressed(java.awt.event.MouseEvent mc) {
                    if (mc.getButton() == java.awt.event.MouseEvent.BUTTON3 && jTable_Registry.getSelectedRowCount() < 1) {
                        java.awt.Point coords = mc.getPoint();
                        int rowNumber = jTable_Registry.rowAtPoint(coords);
                        jTable_Registry.getSelectionModel().setSelectionInterval(rowNumber, rowNumber);
                    }
                }
            };
            jTable_Registry.addMouseListener(jTable_RegistryMouseListener);
            jTable_Registry.setAutoCreateRowSorter(true);
            jTable_Registry.getTableHeader().setReorderingAllowed(false);
            XMLConfig config = new XMLConfig();
            properties = config.readProperties(iface.getAppFunctions().registrations);
            createTableModel(properties);
            setVisible(true);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jCheckBox_Clipboard = new javax.swing.JCheckBox();
        jTextField_Email = new javax.swing.JTextField();
        jTextField_RegKey = new javax.swing.JTextField();
        jButton_Close = new javax.swing.JButton();
        jButton_Generate = new javax.swing.JButton();
        jCheckBox_Email = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable_Registry = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jTextField_Search = new javax.swing.JTextField();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu_File = new javax.swing.JMenu();
        jMenuItem_Exit = new javax.swing.JMenuItem();
        jMenu_Applications = new javax.swing.JMenu();
        jCheckBoxMenuItem_RecycleBinManager = new javax.swing.JCheckBoxMenuItem();
        jMenu_Versions = new javax.swing.JMenu();
        jMenuItem_SetVersion = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTabbedPane1.setFocusable(false);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Banner400x100.png"))); // NOI18N
        jLabel1.setFocusable(false);

        jLabel2.setText("Email Address:");
        jLabel2.setFocusable(false);

        jLabel3.setText("Activatin Code:");
        jLabel3.setFocusable(false);

        jCheckBox_Clipboard.setText("Auto Copy to Clipboard");

        jTextField_Email.setFocusCycleRoot(true);
        jTextField_Email.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_EmailActionPerformed(evt);
            }
        });

        jTextField_RegKey.setEnabled(false);
        jTextField_RegKey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_RegKeyActionPerformed(evt);
            }
        });

        jButton_Close.setText("Close");
        jButton_Close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_CloseActionPerformed(evt);
            }
        });

        jButton_Generate.setText("Generate");
        jButton_Generate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_GenerateActionPerformed(evt);
            }
        });

        jCheckBox_Email.setText("Send Mail");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jCheckBox_Clipboard)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCheckBox_Email)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 161, Short.MAX_VALUE)
                        .addComponent(jButton_Generate)
                        .addGap(18, 18, 18)
                        .addComponent(jButton_Close, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField_RegKey, javax.swing.GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
                            .addComponent(jTextField_Email, javax.swing.GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField_Email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField_RegKey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox_Clipboard)
                    .addComponent(jButton_Close)
                    .addComponent(jButton_Generate)
                    .addComponent(jCheckBox_Email))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Activation", jPanel1);

        jTable_Registry.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Email Address", "Reg Date", "Reg Key"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable_Registry);

        jLabel4.setText("Find User:");

        jTextField_Search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_SearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField_Search, javax.swing.GroupLayout.DEFAULT_SIZE, 542, Short.MAX_VALUE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 606, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField_Search, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jTabbedPane1.addTab("Registry", jPanel2);

        jMenu_File.setText("File");

        jMenuItem_Exit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem_Exit.setText("Exit");
        jMenuItem_Exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_ExitActionPerformed(evt);
            }
        });
        jMenu_File.add(jMenuItem_Exit);

        jMenuBar1.add(jMenu_File);

        jMenu_Applications.setText("Applications");

        jCheckBoxMenuItem_RecycleBinManager.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_1, java.awt.event.InputEvent.CTRL_MASK));
        buttonGroup1.add(jCheckBoxMenuItem_RecycleBinManager);
        jCheckBoxMenuItem_RecycleBinManager.setSelected(true);
        jCheckBoxMenuItem_RecycleBinManager.setText("Recycle Bin Manager");
        jCheckBoxMenuItem_RecycleBinManager.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/RecycledBinManagerIcon.png"))); // NOI18N
        jMenu_Applications.add(jCheckBoxMenuItem_RecycleBinManager);

        jMenuBar1.add(jMenu_Applications);

        jMenu_Versions.setText("Versioning");

        jMenuItem_SetVersion.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem_SetVersion.setText("Set Version");
        jMenuItem_SetVersion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_SetVersionActionPerformed(evt);
            }
        });
        jMenu_Versions.add(jMenuItem_SetVersion);

        jMenuBar1.add(jMenu_Versions);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 611, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField_RegKeyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_RegKeyActionPerformed
        genKey();
    }//GEN-LAST:event_jTextField_RegKeyActionPerformed
    private void jButton_GenerateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_GenerateActionPerformed
        genKey();
    }//GEN-LAST:event_jButton_GenerateActionPerformed
    private void jButton_CloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_CloseActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton_CloseActionPerformed
    private void jTextField_EmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_EmailActionPerformed
        genKey();
    }//GEN-LAST:event_jTextField_EmailActionPerformed
    private void jTextField_SearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_SearchActionPerformed
       String rowStr = "";
       for(int i=0;i < jTable_Registry.getRowCount();i++){
           if(jTable_Registry.getValueAt(i, 0).toString().contains(jTextField_Search.getText())

                   ){
               jTable_Registry.setRowSelectionInterval(i, i);
           }
       }
    }//GEN-LAST:event_jTextField_SearchActionPerformed

    private void jMenuItem_SetVersionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_SetVersionActionPerformed
        String versionStr = javax.swing.JOptionPane.showInputDialog(getSelectedApplication()+"\nVersion Value", "x.xx");
        Double version = Double.valueOf(versionStr);
        String sql = "UPDATE `APP` SET `VERSION` = "+version;
        iface.getDB().Statement(getSelectedApplicationIndex(), sql);
    }//GEN-LAST:event_jMenuItem_SetVersionActionPerformed

    private void jMenuItem_ExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_ExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem_ExitActionPerformed

    private void email() {
        String mailto = "mailto:"+jTextField_Email.getText()+"?subject=\"Activation Certificate: "+getSelectedApplication()+"\"";
        java.io.File file = new java.io.File("C:\\Temp\\development\\NB_Projects\\RecycleBinManager\\RecycleBinManager Website\\regCertificates\\RBMreg_"+jTextField_Email.getText()+".html");
        boolean status = genBody(file);
        if(status){
            status = iface.getSystem().sendMailCustom(mailto,file);
            if(status){
                String sql = "INSERT INTO `LOG` (`ENTRY`,`USER`,`TIMESTAMP`) "+
                      "VALUES ('Activation Sent: "+jTextField_Email.getText()+" for "+getSelectedApplication()+"','Satalink Soft',CURRENT_TIMESTAMP)";
                status = iface.getDB().Statement(0, sql);
                if(properties.containsKey(jTextField_Email.getText()+"^"+getSelectedApplication())){
                    properties.remove(jTextField_Email.getText()+"^"+getSelectedApplication());
                }
                if(!status){
                    jTextField_RegKey.setText("FAILED at MySQL_DB");
                }
            } else {
                jTextField_RegKey.setText("FAILED SENDING EMAIL");
            }
        } else {
            jTextField_RegKey.setText("FAILED GENERATING EMAIL");
        }
    }
    private boolean genBody(java.io.File file){
        String emailhtml =
            "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">"+
            "<html>"+
            "<head>"+
            "<title>"+getSelectedApplication()+" Registration"+jTextField_Email.getText()+"</title>"+
            "</head>"+
            "<body>"+
            "<center>"+
            "<div style=\"WIDTH:625px;BACKGROUND:url('http://www.satalinksoft.com/web_images/RBM_Registration_Certificate.png') no-repeat; HEIGHT: 480px\">"+
            "<br><br><br><p>"+
            "<h1>"+getSelectedApplication()+"</h1>"+
            " <strong>Product Registration Certificate</strong><br>"+
            " <i><a class=\"moz-txt-link-abbreviated\" href=\"http://www.SatalinkSoft.com\">www.SatalinkSoft.com</a></i><br>"+
            " <br>"+
            " <h6>"+
            " <table border=\"0\" width=\"85%\">"+
            "<tbody>"+
            "<tr>"+
            "<td align=\"right\"><b>User Name:</b></td>"+
            "<td align=\"center\"><i>"+jTextField_Email.getText()+"</i></td>"+
            "</tr>"+
            "<tr>"+
            "<td align=\"right\"><b>Activation Key:</b></td>"+
            "<td align=\"center\"><i>"+jTextField_RegKey.getText()+"</i></td>"+
            "</tr>"+
            "</tbody>"+
            "</table>"+
            "<br>"+
            "<table border=\"0\" width=\"85%\">"+
            "<tbody>"+
            "<tr align=\"center\">"+
            "<td align=\"left\">This activation key is encoded specifically for "+jTextField_Email.getText()+". The activation process activates one copy of this product and will require internet access. "+
            "Save this digital certificate for your records.  You may request a new certificate by sending this one to support@satalinksoft.com or by visit our <a href=\"http://www.satalinksoft.com/index.php?p=1_7_SupportCenter\">support center</a> and opening a ticket with the certificate data included."+
            "</td>"+
            "</tr>"+
            "</tbody>"+
            "</table>"+
            "</h6>"+
            "</div>"+
            "</center>"+
            "<script type=\"text/javascript\">"+
            "var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");"+
            "document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));"+
            "</script>"+
            "<script type=\"text/javascript\">"+
            "try {"+
            "var pageTracker = _gat._getTracker(\"UA-10070055-1\");"+
            "pageTracker._trackPageview();"+
            "} catch(err) {}</script>"+
            "</body>"+
            "</html>";
        boolean status = iface.getSystem().fileWriteBytes(file, emailhtml.getBytes(), false);
        return(status);
    }
    private void genKey(){
        if (jTextField_Email.getText().isEmpty()) {
            return;
        }
        String key = iface.getAppFunctions().generateKey(jTextField_Email.getText());
        jTextField_RegKey.setText(key);
        if (jCheckBox_Clipboard.isSelected() && jCheckBox_Email.isSelected()) {
            iface.getAppFunctions().putKeyInClipboard(key);
            email();
        } else if (jCheckBox_Clipboard.isSelected()) {
            iface.getAppFunctions().putKeyInClipboard(key);
        } else if (jCheckBox_Email.isSelected()) {
            email();
        }
        String emailAddy = jTextField_Email.getText();
        String product = getSelectedApplication();
        long timestamp = java.util.Calendar.getInstance().getTimeInMillis();
//        properties.put(emailAddy+"^"+product, emailAddy+"|"+product+"|"+key+"|"+timestamp);
        createTableModel(properties);
        iface.getAppFunctions().writeLog(properties);
    }
    protected String getSelectedApplication(){
        if(jCheckBoxMenuItem_RecycleBinManager.isSelected()){
            application = "Recycle Bin Manager";
        }
        return(application);
    }
    protected int getSelectedApplicationIndex(){
        int index = 0;
        if(jCheckBoxMenuItem_RecycleBinManager.isSelected()){
            index = 1;
        }
        return(index);
    }
    private void jTable_RegistryActionPerformed(java.awt.event.ActionEvent evt) {
        int[] rows = jTable_Registry.getSelectedRows();
        for(int r=(rows.length - 1);r >= 0;r--) {
            if(evt.getActionCommand().equals("Delete")||evt.getActionCommand().equals("Remove")) {
                try {
                    String useract = jTable_Registry.getValueAt(rows[r], 0).toString();
                    String product = jTable_Registry.getValueAt(rows[r], 1).toString();
                    java.util.Map productMap = new java.util.HashMap<Integer, String>();
                    productMap.put("Recycle Bin Manager", 1);
                    jTable_RegistryModel.removeRow(jTable_Registry.convertRowIndexToModel(rows[r]));
                    jTable_Registry.updateUI();
                    if(properties.contains(useract+"^"+product)) properties.remove(useract+"^"+product);
                    int dbID = Integer.valueOf(productMap.get(product).toString());
                    if(iface.getDB().Connect(dbID)){
                        if(iface.getDB().doesValueExist("USERS", "NAME", useract)){
                            String sql = "DELETE FROM USERS WHERE `NAME` = '"+useract+"'";
                            iface.getDB().Statement(dbID, sql);
                        }
                    }
                    XMLConfig config = new XMLConfig();
                    config.writeProperties(properties, iface.getAppFunctions().registrations);
                } catch (java.io.FileNotFoundException ex) {
                    java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (java.io.IOException ex) {
                    java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
            }
        }
    }
    public void createjTable_RecycleBinPopupMenu() {
        javax.swing.JPopupMenu jPopupMenuTable = new javax.swing.JPopupMenu();
        java.awt.Color bg = (java.awt.Color)iface.getSystem().getColorMap().get("menu");
        java.awt.Color fg = (java.awt.Color)iface.getSystem().getColorMap().get("black");
        java.awt.event.ActionListener jTable_RegistyPopupMenuActionListener = new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
        jTable_RegistryActionPerformed(evt);
        }
        };
        javax.swing.JMenuItem menuItemDelete = new javax.swing.JMenuItem();
        menuItemDelete.setText("Delete");
        menuItemDelete.setIcon((javax.swing.Icon)new javax.swing.ImageIcon(iface.getSystem().getResource("/resources/Critical_Icon-16.png")));
        menuItemDelete.setBackground(bg);
        menuItemDelete.setForeground(fg);
        menuItemDelete.addActionListener(jTable_RegistyPopupMenuActionListener);
        jPopupMenuTable.add(menuItemDelete);
        jPopupMenuTable.setBackground(bg);
        jPopupMenuTable.setForeground(fg);
        jPopupMenuTable.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        java.awt.Font font = (java.awt.Font)iface.getSystem().getFontMap().get("GA10");
        jPopupMenuTable.setFont(font);
        jTable_Registry.add(jPopupMenuTable);
        jTable_Registry.setComponentPopupMenu(jPopupMenuTable);
        enableEvents(java.awt.AWTEvent.MOUSE_EVENT_MASK);
    }
    public void createTableModel(java.util.Properties properties) {
        jTable_RegistryModel = new javax.swing.table.DefaultTableModel(new String[]{"Account Name", "Product", "Activation State", "Activation Date"}, 0) {
	    @Override
            public Class getColumnClass(int columnIndex) {
                Class cClass;
                switch (columnIndex) {
                    case 0: cClass = String.class; break;
                    case 1: cClass = String.class; break;
                    case 2: cClass = String.class; break;
                    default: cClass = String.class; break;
                }
                return(cClass);
            }
	    @Override
            public boolean isCellEditable(int rowIndex, int columnIndex){
                return true;
            }
        };
        //Set Table Model
        jTable_Registry.setModel(jTable_RegistryModel);
        javax.swing.ToolTipManager.sharedInstance().setEnabled(false);
        CustomTableDataRenderer custom = new CustomTableDataRenderer();
        jTable_Registry.getTableHeader().setDefaultRenderer(new CustomHeaderRenderer());
        int cols = jTable_Registry.getModel().getColumnCount();
        for (int i=0;i < cols;i++) {
            jTable_Registry.getColumnModel().getColumn(i).setCellRenderer(custom);
        }
        jTable_Registry.setModel(jTable_RegistryModel);
        createjTable_RecycleBinPopupMenu();
        String sql = "SELECT * FROM USERS";
        java.util.ArrayList RBMusers = iface.getDB().Select(1, sql);
        for(int i=0;i < RBMusers.size();i++){
            java.util.Map map = (java.util.Map)RBMusers.get(i);
            Object[] object = {
                map.get("NAME"),
                "Recycle Bin Manager",
                map.get("STATE"),
                map.get("TIMESTAMP")
            };
            jTable_RegistryModel.addRow(object);
        }
        java.util.Iterator iterator = properties.keySet().iterator();
        while(iterator.hasNext()){
            String key = iterator.next().toString();
            String[] values = properties.getProperty(key).split("\\|");
            long timestamp = Long.valueOf(values[3]);
            String datestamp = iface.getSystem().getCalendarDate(timestamp);
            Object[] object = {
              values[0],
              values[1],
              values[2],
              datestamp
            };
            jTable_RegistryModel.addRow(object);
        }

    }

//Internal Classes
class CustomHeaderRenderer extends javax.swing.table.DefaultTableCellRenderer {
    Sys sys = new Sys();
    java.util.Map colorMap = sys.getColorMap();
    java.util.Map fontMap = sys.getFontMap();
    java.awt.Rectangle paintingRect = null,lpr = null;
    java.awt.GradientPaint gp = null, hoverGradient, columnGradient;

    public CustomHeaderRenderer() {
        super();
        setOpaque(false);
        setFont((java.awt.Font)fontMap.get("GA16B"));
        java.awt.Color menu = (java.awt.Color)colorMap.get("menu");
        java.awt.Color head = (java.awt.Color)colorMap.get("header");
        javax.swing.border.BevelBorder border = (javax.swing.border.BevelBorder) javax.swing.BorderFactory.createBevelBorder(
                javax.swing.border.BevelBorder.RAISED,
                menu,
                menu.brighter(),
                head,
                head.brighter());
                setBorder((border));
    }
    public void setColumnGradient(java.awt.GradientPaint gp) {
        this.columnGradient = gp;
    }
    public java.awt.GradientPaint getColumnGradient() {
        return columnGradient;
    }
    @Override
    public void paintComponent(java.awt.Graphics g) {
        java.awt.Rectangle rect = paintingRect;
        java.awt.Graphics2D g2 = (java.awt.Graphics2D)g;
            g2.setPaint(gp);
            g2.fillRect( 0, 0, rect.width, rect.height );
        super.paintComponent(g);
    }
    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected,
        boolean hasFocus, int row, int col) {
        java.awt.Color head = (java.awt.Color)colorMap.get("header");
        int[] colAlignments = new int[]{2,2,2,2,2};
        if(colAlignments != null) setHorizontalAlignment(colAlignments[col]);
        java.awt.Rectangle rect = table.getTableHeader().getHeaderRect(col);
        gp = new java.awt.GradientPaint(0, 0, head.brighter(), 0, rect.height/2, head);
        setForeground((java.awt.Color)colorMap.get("black"));
        paintingRect = rect;
        setText( value == null ? "" : value.toString() );
        javax.swing.ImageIcon icon = new javax.swing.ImageIcon();
        setIcon(icon);
        setIconTextGap(6);
        return(this);
    }
}
class CustomTableDataRenderer extends javax.swing.table.DefaultTableCellRenderer {
    java.awt.Rectangle paintingRect = null;
    java.awt.GradientPaint gp = null;
    public CustomTableDataRenderer() {
        super();
        setOpaque(false);
        java.util.Map colorMap = iface.getSystem().getColorMap();
        java.awt.Color menu = (java.awt.Color) colorMap.get("menu");
        java.awt.Color header = (java.awt.Color) colorMap.get("header");
        javax.swing.border.BevelBorder border = (javax.swing.border.BevelBorder) javax.swing.BorderFactory.createBevelBorder(
                javax.swing.border.BevelBorder.RAISED,
                menu,
                menu.brighter(),
                header,
                header.brighter());
        setBorder((border));
    }
    @Override
    public java.awt.Component getTableCellRendererComponent(
         javax.swing.JTable jt,
         Object value,
         boolean isSelected,
         boolean hasFocus,
         int row,
         int column
         ){
        java.util.Map colorMap = iface.getSystem().getColorMap();
        java.util.Map fontMap = iface.getSystem().getFontMap();
        java.awt.Component comp = super.getTableCellRendererComponent(jt,value,isSelected,hasFocus,row,column);
        java.awt.Color tabColor = (java.awt.Color)colorMap.get("skyblue");
        javax.swing.ImageIcon icon = new javax.swing.ImageIcon();
        setIcon(icon);
        setIconTextGap(6);
        java.awt.Rectangle rect = jt.getCellRect(row, column, isSelected);
        paintingRect = rect;
        setFont((java.awt.Font)fontMap.get("GA11B"));
        setForeground((java.awt.Color)colorMap.get("black"));
        int[] sels = jt.getSelectedRows();
        if(row % 2 != 0) {
                tabColor = (java.awt.Color)colorMap.get("cloudwhite");
        } else if(isSelected) {
            if(value instanceof String) setFont((java.awt.Font)fontMap.get("GA14B"));
            if(rect != null) {
                    if(sels[0] == row || sels.length == 1) {
                        gp = new java.awt.GradientPaint(0,0,tabColor.darker(),0,rect.height/2,tabColor.brighter());
                    } else if(sels[jt.getSelectedRowCount() - 1] == row) {
                        gp = new java.awt.GradientPaint(0,0,tabColor.brighter(),0,rect.height/2,tabColor.darker());
                    } else {
                        gp = new java.awt.GradientPaint(0,0,tabColor.brighter(),0,rect.height/2,tabColor.brighter());
                    }
                }
        } else if(!isSelected) {
            if(rect != null) {
                gp = new java.awt.GradientPaint(0,0,tabColor.brighter(),(rect.width * 7)/8,0,tabColor.darker());
            }
        }
        int[] colAlignments = new int[]{2,4,0,2,2};
        setHorizontalAlignment(colAlignments[column]);
        // ToolTip  (Middle MouseWheel Button Activates)
        java.awt.Color bg = (java.awt.Color)colorMap.get("menu");
        if(value != null){
            String tooltext =   "<HTML><BODY>"+
                                "<TABLE BORDER CELLPADDING=8 BGCOLOR=\"#"+Integer.toHexString(bg.getRGB()&0x00ffffff)+"\">"+
                                "<TH><H3>"+
                                jt.getModel().getColumnName(column)+
                                "</H3></TH><TD WIDTH=500><P STYLE=\"word-wrap:break-word;width:100%;left:0\"><B>"+
                                value.toString()+
                                "</B></P></TD></TABLE></BODY></HTML>";
            setToolTipText(tooltext);
        }
        javax.swing.ToolTipManager.sharedInstance().setInitialDelay(3750);
        javax.swing.ToolTipManager.sharedInstance().setDismissDelay(30000);
        javax.swing.ToolTipManager.sharedInstance().setReshowDelay(1200);
        return(comp);
    }
    public java.awt.GradientPaint getColumnGradient() {
        return gp;
    }
    @Override
    public void paintComponent(java.awt.Graphics g) {
        java.awt.Rectangle rect = paintingRect;
        java.awt.Graphics2D g2 = (java.awt.Graphics2D)g;
        g2.setPaint(gp);
        g2.fillRect( 0, 0, rect.width, rect.height );
        super.paintComponent(g);
    }
 }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton_Close;
    private javax.swing.JButton jButton_Generate;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem_RecycleBinManager;
    private javax.swing.JCheckBox jCheckBox_Clipboard;
    private javax.swing.JCheckBox jCheckBox_Email;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem_Exit;
    private javax.swing.JMenuItem jMenuItem_SetVersion;
    private javax.swing.JMenu jMenu_Applications;
    private javax.swing.JMenu jMenu_File;
    private javax.swing.JMenu jMenu_Versions;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable_Registry;
    private javax.swing.JTextField jTextField_Email;
    private javax.swing.JTextField jTextField_RegKey;
    private javax.swing.JTextField jTextField_Search;
    // End of variables declaration//GEN-END:variables

}
