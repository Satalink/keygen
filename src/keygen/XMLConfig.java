
package keygen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class XMLConfig {

    public Properties readProperties(File xmlfile) throws Exception {
        Properties properties = new Properties();
        FileInputStream in = new FileInputStream(xmlfile.getAbsolutePath());
        properties.loadFromXML(in); 
        return properties;
    }
        
    public void writeProperties(Properties properties, File xmlfile) throws FileNotFoundException, IOException {
        FileOutputStream out = new FileOutputStream(xmlfile.getAbsolutePath());
        properties.storeToXML(out, "UTF-8");
    }
}

