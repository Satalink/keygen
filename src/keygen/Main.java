package keygen;

public class Main implements Interface{
    AppFunctions func;
    GUI gui;
    Sys system;
    MySQL_DB database;
    private Main() {
        system = new Sys();
        database = new MySQL_DB(this);
        func = new AppFunctions(this);
        gui = new GUI(this);
    }

    private Main(String[] emails) {
        try {
            system = new Sys();
            func = new AppFunctions(this);
            XMLConfig config = new XMLConfig();
            java.util.Properties properties = config.readProperties(func.registrations);
            for (int i = 0; i < emails.length; i++) {
                String email = emails[i].split("^")[0];
                String product = emails[i].split("^")[1];
                String key = func.generateKey(email);
                long timestamp = System.currentTimeMillis();
                properties.put(email+"^"+product, email+"|"+product+"|"+key+"|"+timestamp);
                Thread.sleep(1000);
            }
            func.writeLog(properties);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

    }

    public static void main(String[] args) {
        //args[0]  = email address
        if(args.length == 0) {
            Main main = new Main();
        } else {
            Main main = new Main(args);
        }
    }

    public MySQL_DB getDB(){
        return(database);
    }
    public GUI getGUI() {
        return(gui);
    }
    public AppFunctions getAppFunctions(){
        return(func);
    }
    public Sys getSystem(){
        return(system);
    }

}


