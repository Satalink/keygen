package keygen;

import java.sql.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MySQL_DB {
    Interface iface;
    private String DBDriver = "com.mysql.jdbc.Driver";
    protected Connection conn;
    String[] urls = {"jdbc:mysql://www.satalinksoft.com/satalin1_DBLog",
                     "jdbc:mysql://www.satalinksoft.com/satalin1_RBM",
                    };

    public MySQL_DB(Interface face){
        iface=face;face=null;
    }

    public boolean Connect(int dbID) {
        try {
            Class.forName(DBDriver).newInstance();
            conn = DriverManager.getConnection(urls[dbID], "satalin1", "29yx11b");
            return(conn.isValid(30000));
        } catch (Exception ex) {
            Logger.getLogger(MySQL_DB.class.getName()).log(Level.SEVERE, null, ex);
            return(false);
        }
    }
    public boolean Close() {
        boolean status = false;
        try {
            if (conn != null) {
                conn.close();
                status = true;
                conn = null;
            }
            return(status);
        }
        catch (SQLException ex) {
            Logger.getLogger(MySQL_DB.class.getName()).log(Level.SEVERE, null, ex);
            return(status);
        }
    }


    public ArrayList Select(int dbID, String sqlstatement) {
        ArrayList<Map> resultList = new ArrayList<Map>();
        try {
            if(conn != null) Close();
            if(conn == null || conn.isClosed()) {
                Connect(dbID);
            }
            if(conn.isValid(30000)) {
                Statement stmt = conn.createStatement();
                ResultSet resultset = stmt.executeQuery(sqlstatement);
                ResultSetMetaData rsmd = resultset.getMetaData();
                int columns = rsmd.getColumnCount();
                int i = 0;
                while (resultset.next()) {
                    int col = 1;
                    Map resultMap = new HashMap();
                    while (col <= columns) {
                        String value = resultset.getString(col);
                        String colName = rsmd.getColumnName(col);
                        resultMap.put(colName, value);
                        col++;
                    }
                    resultList.add(0, resultMap);
                    i++;
                }
                resultset.close();
                stmt.close();
                Close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(MySQL_DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultList;
    }
    public boolean Statement(int dbID,String sqlstatement) {
        boolean status = false;
        try {
            if (conn != null) {
                Close();
            }
            if(Connect(dbID))
                conn.createStatement().execute(sqlstatement);
                status = true;
            } catch (SQLException ex) {
                Logger.getLogger(MySQL_DB.class.getName()).log(Level.SEVERE, sqlstatement, ex);
                status = false;
            }
            return status;
    }

    public int selectCount(String table, String column) {
	int resultvalue = 0;
        String sql = "SELECT COUNT("+column+") from "+table;
	try {
		Statement stmt = conn.createStatement();
		ResultSet results = stmt.executeQuery(sql);
                if(results.next()) {
		    resultvalue = results.getInt(1);
		}
		results.close();
		stmt.close();
	} catch (SQLException ex) {
		resultvalue = 0;
	}
	return(resultvalue);
    }
    public boolean doesValueExist(String table, String key, String value){
        boolean exists = false;
        int count = 0;
        try {
            if(selectCount(table,key) > 0){
                String sql = "SELECT COUNT(*) FROM "+table+" WHERE `"+key+"` = '"+value+"'";
                Statement stmt = conn.createStatement();
                ResultSet result = stmt.executeQuery(sql);
                if(result.next()) {
                    count = result.getInt(1);
                }
                if(count > 0){
                    exists = true;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(MySQL_DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return(exists);
    }

}
