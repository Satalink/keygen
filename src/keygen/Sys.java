package keygen;

public class Sys {

    private java.util.Map<String, javax.swing.Icon> iconMap = new java.util.HashMap<String, javax.swing.Icon>();
    private javax.swing.JFileChooser fc = new javax.swing.JFileChooser();
//Internal
    public String getJavaVersion() {
        String jVersion = System.getProperty("java.version");
        return(jVersion);
    }
    public String getIPaddress() {
        String ipAddr = "Unknown";
        try {
            ipAddr = java.net.InetAddress.getLocalHost().getHostAddress();
        } catch (java.net.UnknownHostException ex) {
        }
        return(ipAddr);
    }
    public String getUserHome() {
        String usrHome = System.getProperty("user.home");
        return(usrHome);
    }
    public String getUserName() {
        String osUser = System.getProperty("user.name");
        return(osUser);
    }
    public String getOS() {
        return(System.getProperty("os.name"));
    }
    public int genRandomInt(int x){
        java.util.Random random = new java.util.Random();
        return(random.nextInt(x));
    }
    public int genRandomInt(int min, int max){
        java.util.Random random = new java.util.Random();
        int value = -1;
        while(value < min){
            value = random.nextInt(max);
        }
        return(value);
    }
    public int getPercentComplete(int a, int b) {
        int pct = (a*100/b);
        return(pct);
    }
    public java.net.URL getResource(String reference) {
        return(Class.class.getResource(reference));
    }
    public String getHostname() {
        String hostname = null;
                java.net.InetAddress addr = null;
            try {
                addr = java.net.InetAddress.getLocalHost();
            } catch (java.net.UnknownHostException ex) {
                java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        hostname = addr.getHostName();
        return(hostname);
    }
    public void execCMD(String cmd) {
        try {
            Process process = Runtime.getRuntime().exec(cmd);
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    public boolean sendMailCustom(String mailto,java.io.File url) {
        boolean status = false;
        if(getOS().startsWith("Windows")){
            try {
                Runtime.getRuntime().exec("C:\\Program Files\\Sylpheed\\sylpheed.exe --compose "+mailto+" --attach \""+url.getAbsolutePath()+"\"");
                status = true;
            } catch (java.io.IOException ex) {
                java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
        return(status);
    }
    public void launchBrowser(String url) {
        if(java.awt.Desktop.isDesktopSupported()){
            java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
            try {
                desktop.browse(java.net.URI.create(url));
            } catch (java.io.IOException ex) {
                java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
    }
    public void sendMail(String url){
        if(java.awt.Desktop.isDesktopSupported()){
            java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
            try {
                desktop.mail(java.net.URI.create(url));
            } catch (java.io.IOException ex){
                java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
    }
    public void editFile(java.io.File file){
        if(java.awt.Desktop.isDesktopSupported()){
            java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
            try {
                desktop.edit(file);
            } catch (java.io.IOException ex){
                java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
    }
//Time
    public String getDateStamp(){
        java.util.Calendar timestamp = new java.util.GregorianCalendar();
        java.text.NumberFormat intformat = new java.text.DecimalFormat("00");
        String DD = intformat.format(timestamp.get(java.util.Calendar.DAY_OF_MONTH));
        String MM = intformat.format(timestamp.get(java.util.Calendar.MONTH) + 1);
        String YY = intformat.format(timestamp.get(java.util.Calendar.YEAR));
        String dateStamp = YY+MM+DD;
        return(dateStamp);
    }
    public Long getEpoch() {
        long epoch = java.util.Calendar.getInstance().getTimeInMillis();
        return(epoch);
    }
    public long getEpoch(String datetime) {
        try {
            long epoch = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(datetime).getTime();
            return epoch;
        } catch (java.text.ParseException ex) {
            java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            return(-1);
        }
    }
    public String getTimeStamp() {
        String timestamp = new java.sql.Timestamp(getEpoch()).toString();
        return(timestamp);
    }
    public String getTimeStamp_YY_MM_DD_hh_mm_ss(){
        java.util.Calendar timestamp = new java.util.GregorianCalendar();
        java.text.NumberFormat intformat = new java.text.DecimalFormat("00");
        String DD = intformat.format(timestamp.get(java.util.Calendar.DAY_OF_MONTH));
        String MM = intformat.format(timestamp.get(java.util.Calendar.MONTH) + 1);
        String YY = intformat.format(timestamp.get(java.util.Calendar.YEAR));
        String hh = intformat.format(timestamp.get(java.util.Calendar.HOUR_OF_DAY));
        String mm = intformat.format(timestamp.get(java.util.Calendar.MINUTE));
        String ss = intformat.format(timestamp.get(java.util.Calendar.SECOND));
        String timeStamp = YY+"-"+MM+"-"+DD+" "+hh+":"+mm+":"+ss;
        return(timeStamp);
    }
    public String getCalendarDate(long longtime) {
                java.util.Date mtime = new java.util.Date(longtime);
                String timestring = java.text.DateFormat.getDateInstance(java.text.DateFormat.MEDIUM).format(mtime)+" "+java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT).format(mtime);
                return(timestring);
    }

//File
    public String getFileExtension(java.io.File file) {
        String[] pts = file.getName().split("\\.");
        String ext = pts[(pts.length - 1)].toLowerCase();
        return(ext);
    }
    public javax.swing.JFileChooser getFileChooser() {
        return(fc);
    }
    public java.util.ArrayList getFileOpen(java.io.File file){
            java.util.ArrayList fileBufferList = new java.util.ArrayList();
            try {
                if(!file.canRead()) file.setReadable(true);
                java.io.FileInputStream fis = new java.io.FileInputStream(file);
                byte[] buf = new byte[1024];
                int i = 0;
                while ((i = fis.read(buf)) != -1) {
                    fileBufferList.add(buf);
                    i--;
                }
                fis.close();
            } catch (java.io.IOException ex) {
                java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            return(fileBufferList);
    }
    public long getFileSize(java.io.File file) {
        long size = 0;
        if(file.isFile()) {
            size = file.length();
        } else if(file.isDirectory()) {
            long tsize = 0;
            java.io.File[] flist = file.listFiles();
            for (int i=0;i < flist.length;i++) {
                java.io.File tfile = new java.io.File(flist[i].getAbsolutePath());
                if(tfile.isFile()) {
                    tsize+=tfile.length();
                } else {
                    tsize+=getFileSize(tfile);
                }
             }
             size = tsize;
        }
        return(size);
    }
    public String getFileSize(long size) {
        String unit = null;
        float product = (float) 0.00;
        java.text.NumberFormat decformat = new java.text.DecimalFormat("0.00");
        unit = " MB";
        product = size;
        String sizeStr = new String(decformat.format(product))+" "+unit;
        return(sizeStr);
    }
    public java.util.Map filePermisions(java.io.File file) {
        java.util.Map perms = null;
        if(file.exists()) {
            perms.put("exists", file.exists());
            perms.put("isDirectory", file.isDirectory());
            perms.put("isHidden", file.isHidden());
            perms.put("canRead", file.canRead());
            perms.put("canWrite", file.canWrite());
            perms.put("canExecute", file.canExecute());
        } else {
            perms.put("exists", file.exists());
        }
        return(perms);
    }
    public boolean fileWriteBytes(java.io.File file, byte[] bytes, boolean append){
        java.io.FileOutputStream fos = null;
        boolean status = false;
        try {
            fos = new java.io.FileOutputStream(file, append);
            fos.write(bytes);
            status = true;
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (java.io.IOException ex) {
                java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
        return status;
    }

    public java.util.ArrayList getRecursiveFiles(java.io.File file) {
        java.util.ArrayList fileList = new java.util.ArrayList();
        if (file.isDirectory()) {
            String[] children = file.list();
                for (int i=0;i<children.length;i++) {
                    fileList.addAll(getRecursiveFiles(new java.io.File(file, children[i])));
            }
        } else if (file.isFile()) fileList.add(file);
        return(fileList);
    }
    public boolean getClipboardAccess() {
        boolean sma = false;
            SecurityManager sm = System.getSecurityManager();
            try {
              if (sm != null) {
                sm.checkSystemClipboardAccess();
              }
              sma = true;
            } catch (SecurityException ex) {
              sma = false;
            }
        return(sma);
    }
    public javax.swing.Icon getSystemFileIcon(java.io.File file) {
        String ext = "";
        if(file.getName().contains(".")) {
            String[] exts = file.getName().split("\\.");
            ext = exts[(exts.length - 1)].toLowerCase();
        }
        java.util.Map exceptionMap = new java.util.HashMap<String, String>();
        exceptionMap.put("exe", "Executable File Icons");
        exceptionMap.put("lnk", "Shortcuts");
        exceptionMap.put("url", "Web Links");
        if(iconMap.containsKey(ext)) {
            return(iconMap.get(ext));
        } else if(exceptionMap.containsKey(ext)) {
            javax.swing.Icon icon = fc.getFileSystemView().getSystemIcon(file);
            iconMap.put(file.getName().toString(), icon);
            return(icon);
        } else {
            javax.swing.Icon icon = fc.getFileSystemView().getSystemIcon(file);
            iconMap.put(ext, icon);
            return(icon);
        }
    }
    public boolean copyFile (java.io.File fromFile, java.io.File toFile) throws java.io.FileNotFoundException {
        boolean status = false;
        if(!fromFile.exists()) return(status);
        try {
            java.io.FileInputStream fis = new java.io.FileInputStream(fromFile);
            java.io.FileOutputStream fos = new java.io.FileOutputStream(toFile);
            byte[] buf = new byte[1024];
            int i = 0;
            while ((i = fis.read(buf)) != -1) {
            fos.write(buf, 0, i);
            }
            fis.close();
            fos.close();
            if (toFile.exists()) {
            status = true;
            }
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        return(status);
    }
    public boolean moveFile (java.io.File fromFile, java.io.File toFile) {
        boolean status = false;
        fromFile.renameTo(toFile);
        return(status);
    }
    public boolean deleteFile(java.io.File file) {
        boolean status = true;
        if (file.isDirectory()) {
            String[] children = file.list();
            for (int i=0; i<children.length; i++) {
                java.io.File dfile = new java.io.File(children[i]);
                if(dfile.exists() && dfile.isFile() && dfile.setWritable(true, true)) {
                    status = deleteFile(dfile);
                } else if(dfile.exists() && dfile.isDirectory() && dfile.setWritable(true, true)) {
                    status = deleteFile(dfile);
                }
            }
        }
        // The directory is now empty so delete it
        if(status) {
            status = file.delete();
        }
        return(status);
    }
    public boolean renameFile(java.io.File file, java.io.File dest){
        return(file.renameTo(dest));
    }

//Colors & Fonts
    public java.util.Map getColorMap(){
        java.util.Map colorMap = new java.util.HashMap<String, java.awt.Color>();
        colorMap.put("black", new java.awt.Color(0,0,0));
        colorMap.put("white", new java.awt.Color(255,255,255));
        colorMap.put("header", new java.awt.Color(200,200,200));
        colorMap.put("menu", new java.awt.Color(220,220,220));
        colorMap.put("skyblue", new java.awt.Color(135,206,235));
        colorMap.put("cloudwhite", new java.awt.Color(209,238,238));
        return(colorMap);
    }
    public java.util.Map getFontMap(){
        java.util.Map fontMap = new java.util.HashMap<String, java.awt.Font>();
        fontMap.put("GA10", new java.awt.Font("Georgia", 0, 10));
        fontMap.put("GA10B", new java.awt.Font("Georgia", 1, 10));
        fontMap.put("GA11", new java.awt.Font("Georgia", 0, 11));
        fontMap.put("GA11B", new java.awt.Font("Georgia", 1, 11));
        fontMap.put("GA12B", new java.awt.Font("Georgia", 1, 12));
        fontMap.put("GA14", new java.awt.Font("Georgia", 0, 14));
        fontMap.put("GA14B", new java.awt.Font("Georgia", 1, 14));
        fontMap.put("GA16", new java.awt.Font("Georgia", 0, 16));
        fontMap.put("GA16B", new java.awt.Font("Georgia", 1, 16));
        return(fontMap);
    }

//Process
     public String[] sortArray(String[] strArray) {
       if (strArray.length == 1) return(strArray);   // no need to sort one item
       java.text.Collator collator = java.text.Collator.getInstance();
       String strTemp;
       for (int i=0;i<strArray.length;i++) {
         for (int j=i+1;j<strArray.length;j++) {
           if (collator.compare(strArray[i], strArray[j]) > 0) {
             strTemp = strArray[i];
             strArray[i] = strArray[j];
             strArray[j] = strTemp;
           }
         }
       }
       return(strArray);
     }
     public javax.swing.JList sortList(javax.swing.JList list) {
       javax.swing.ListModel model = list.getModel();
       int listSize = model.getSize();
       String[] array = new String[listSize];
       for (int i=0;i<listSize;i++) {
         array[i] = (String)model.getElementAt(i);
       }
       sortArray(array);
       list.setListData(array);
       list.revalidate();
       return(list);
     }
    
//System OS Windows
    public String readRegistry(String location) {
        String value = null;

        return(value);
    }
}
