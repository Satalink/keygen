package keygen;

public interface Interface {
    public GUI getGUI();
    public AppFunctions getAppFunctions();
    public Sys getSystem();
    public MySQL_DB getDB();
}
